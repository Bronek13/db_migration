<?php

namespace App\Entity\Source;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserDetailRepository")
 * @ORM\Table(name="user_details")
 */
class UserDetail
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="NAME")
     */
    private $name;

    /**
     * @ORM\Column(type="string", name="SURNAME")
     */
    private $surname;

    /**
     * @ORM\Column(type="string", name="EMAIL ")
     */
    private $email;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, name="DATA")
     */
    private $data;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, name="DATA2")
     */
    private $data2;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return UserDetail
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     * @return UserDetail
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return UserDetail
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     * @return UserDetail
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getData2()
    {
        return $this->data2;
    }

    /**
     * @param mixed $data2
     * @return UserDetail
     */
    public function setData2($data2)
    {
        $this->data2 = $data2;
        return $this;
    }
}
