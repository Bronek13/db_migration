<?php

namespace App\Entity\Destination;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CustomerDetailRepository")
 * @ORM\Table(name="customer_details")
 */
class CustomerDetail
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @ORM\Column(columnDefinition="CHAR(255)", name="FULLNAME ")
     */
    private $fullName;

    /**
     * @ORM\Column(columnDefinition="CHAR(255)", name="E_MAIL ")
     */
    private $email;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, name="BALANCE")
     */
    private $balance;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, name="TOTALPURCHASE")
     */
    private $totalPurchase;

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param mixed $fullName
     * @return CustomerDetail
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return CustomerDetail
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param mixed $balance
     * @return CustomerDetail
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalPurchase()
    {
        return $this->totalPurchase;
    }

    /**
     * @param mixed $totalPurchase
     * @return CustomerDetail
     */
    public function setTotalPurchase($totalPurchase)
    {
        $this->totalPurchase = $totalPurchase;
        return $this;
    }
}
