<?php

namespace App\Repository;

use App\Entity\Destination\CustomerDetail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CustomerDetail|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomerDetail|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomerDetail[]    findAll()
 * @method CustomerDetail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerDetailRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CustomerDetail::class);
    }
}
