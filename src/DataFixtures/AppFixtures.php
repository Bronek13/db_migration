<?php

namespace App\DataFixtures;

use App\Entity\Source\UserDetail;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    const USERS_AMOUNT = 200;
    const MULTIPLIER = 100;
    const MIN_DATA_LENGTH = 0;
    const MAX_DATA_LENGTH = 100000000;
    const MIN_NAME_LENGTH = 5;
    const MAX_NAME_LENGTH = 10;
    const ALPHABET = 'abcdefghijklmnopqrstuvwxyz';

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < self::USERS_AMOUNT; $i++) {
            $userDetails = new UserDetail();
            $userDetails->setName(ucfirst($this->generateString()));
            $userDetails->setSurname(ucfirst($this->generateString()));
            $userDetails->setData($this->randNumeric());
            $userDetails->setData2($this->randNumeric());
            $userDetails->setEmail(
                $this->generateString()
                . '@' . $this->generateString() . '.com'
            );

            $manager->persist($userDetails);
        }

        $manager->flush();
    }

    private function randNumeric()
    {
        return mt_rand (
                self::MIN_DATA_LENGTH * self::MULTIPLIER,
                self::MAX_DATA_LENGTH * self::MULTIPLIER
            ) / self::MULTIPLIER;
    }

    private function generateString()
    {
        $length = mt_rand (
            self::MIN_NAME_LENGTH,
            self::MAX_NAME_LENGTH
        );

        return substr(str_shuffle(self::ALPHABET),1,$length);
    }
}
