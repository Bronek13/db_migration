<?php

namespace App\Command;

use App\Entity\Destination\CustomerDetail;
use App\Entity\Source\UserDetail;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MigrateDatabaseCommand extends Command
{
    use LockableTrait;

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Migrate data from source db to destination db.')
            ->setName('app:migrate:database')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return 0;
        }
        $start = new \DateTime();

        /** @var EntityManagerInterface $sourceEm */
        $sourceEm = $this->container->get('doctrine')->getManager('source');
        /** @var EntityManagerInterface $destinationEm */
        $destinationEm = $this->container->get('doctrine')->getManager('destination');

        $userDetailsRepo = $sourceEm->getRepository(UserDetail::class);

        // todo i dont know if there should be truncate every time, add checking for same entries?
//        $customerDetailsRepo = $destinationEm->getRepository(CustomerDetail::class);
//
//        $customersDetails = $customerDetailsRepo->findAll();
//
//        foreach ($customersDetails as $customerDetails) {
//            $destinationEm->remove($customerDetails);
//        }

        /** @var UserDetail[] $usersDetails */
        $usersDetails = $userDetailsRepo->findAll();

        foreach ($usersDetails as $userDetail) {
            $customerDetail = new CustomerDetail();
            $customerDetail->setFullName(
                $userDetail->getName() . ' ' . $userDetail->getSurname()
            );
            $customerDetail->setEmail($userDetail->getEmail());
            $customerDetail->setBalance($userDetail->getData());
            $customerDetail->setTotalPurchase($userDetail->getData2());

            $destinationEm->persist($customerDetail);
        }

        $destinationEm->flush();

        $end = new \DateTime();
        $difference = $start->diff($end);

        $output->writeln("MIGRATION SUCCESSFUL \n" . 'Execution time: '
            . $difference->format('%H') . ' h ' . $difference->format('%I')
            . ' min ' . $difference->format('%S') . ' s'
        );

        $this->release();

        return true;
    }
}