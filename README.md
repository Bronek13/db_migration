# Command
```
php bin/console app:migrate:database
```
##

### Config
#### 1. run
```
php composer.phar install
```
##

#### databases configured on remote server
remotemysql.com

##

#### auto fill source db with random data with command
```
php bin/console doctrine:fixtures:load
```

